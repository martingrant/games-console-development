#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cmath>
#include <stdint.h>
#include <string>
#include <fstream>

#include "MemoryMapping/MemoryMapped.h"
#include "TGA\Image.h"

uint8_t output[480][640];

// Create array for initial data
uint_fast8_t dataGrid[480][640];

TGAImage *img = new TGAImage(640, 480);

void writeTGA(const char* fileName, uint8_t(&pArray)[480][640]);
void writeTGAFinal(const char* fileName, uint8_t(&pArray)[480][640]);

struct box
{
	int x;
	int y;
	int w;
	int h;
};

std::vector<box*> boxVector;

void sobel(uint8_t(&dataGrid)[480][640])
{
	int_fast8_t GX[3][3];
	int_fast8_t GY[3][3];
	//uint16_t output[480][640];
	int_fast16_t gxVal;
	int_fast16_t gyVal;
	uint_fast8_t sobelValue;

	/* 3x3 GX Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
	GX[0][0] = -1; GX[0][1] = 0; GX[0][2] = 1;
	GX[1][0] = -2; GX[1][1] = 0; GX[1][2] = 2;
	GX[2][0] = -1; GX[2][1] = 0; GX[2][2] = 1;

	/* 3x3 GY Sobel mask.  Ref: www.cee.hw.ac.uk/hipr/html/sobel.html*/
	GY[0][0] = 1; GY[0][1] = 2; GY[0][2] = 1;
	GY[1][0] = 0; GY[1][1] = 0; GY[1][2] = 0;
	GY[2][0] = -1; GY[2][1] = -2; GY[2][2] = -1;

	for (int rows = 0; rows < 480; ++rows)
	{
		for (int cols = 0; cols < 640; ++cols)
		{
			//middle pixel
			gxVal = dataGrid[rows][cols] * GX[1][1];
			gyVal = dataGrid[rows][cols] * GY[1][1];
			//above middle pixel
			gxVal += dataGrid[rows - 1][cols] * GX[0][1];
			gyVal += dataGrid[rows - 1][cols] * GY[0][1];
			//top left pixel
			gxVal += dataGrid[rows - 1][cols - 1] * GX[0][0];
			gyVal += dataGrid[rows - 1][cols - 1] * GY[0][0];
			//left middle pixel
			gxVal += dataGrid[rows][cols - 1] * GX[1][0];
			gyVal += dataGrid[rows][cols - 1] * GY[1][0];
			//bottom left pixel
			gxVal += dataGrid[rows + 1][cols - 1] * GX[2][0];
			gyVal += dataGrid[rows + 1][cols - 1] * GY[2][0];
			//below middle pixel
			gxVal += dataGrid[rows + 1][cols] * GX[2][1];
			gyVal += dataGrid[rows + 1][cols] * GY[2][1];
			//bottom right pixel
			gxVal += dataGrid[rows + 1][cols + 1] * GX[2][2];
			gyVal += dataGrid[rows + 1][cols + 1] * GY[2][2];
			//middle right pixel
			gxVal += dataGrid[rows][cols + 1] * GX[1][2];
			gyVal += dataGrid[rows][cols + 1] * GY[1][2];
			//top right pixel
			gxVal += dataGrid[rows + 1][cols - 1] * GX[0][2];
			gyVal += dataGrid[rows + 1][cols - 1] * GY[0][2];

			gxVal = gxVal / 9;
			gyVal = gyVal / 9;
			sobelValue = sqrt(pow(gxVal, 2) + pow(gyVal, 2));
			output[rows][cols] = sobelValue;
		}
	}

	writeTGA("sobel.tga", output);
}

//void median()
//{
//	int fuckyall[300000];
//	int fuckyall2[480][640];
//	uint16_t median[49];
//	uint16_t sortedMedian[49];
//	uint16_t medianOutput[480][640];
//	signed char rowToAdd = -3;
//	int i = 0;
//	for (int rows = 0; rows < 480; ++rows)
//	{
//		for (int cols = 0; cols < 640; ++cols)
//		{
//			i = 0;
//			while (rowToAdd != 3)
//			{
//				median[i] = output[rows + rowToAdd][cols - 3];
//				++i;
//				median[i] = output[rows + rowToAdd][cols - 2];
//				++i;
//				median[i] = output[rows + rowToAdd][cols - 1];
//				++i;
//				median[i] = output[rows + rowToAdd][cols];
//				++i;
//				median[i] = output[rows + rowToAdd][cols + 1];
//				++i;
//				median[i] = output[rows + rowToAdd][cols + 2];
//				++i;
//				median[i] = output[rows + rowToAdd][cols + 3];
//				++i;
//				++rowToAdd;
//			}
//			rowToAdd = -3;
//
//			for (int i = 0; i < 49; ++i)
//			{
//				if (median[i] > 49)
//				{
//					median[i] = 0;
//				}
//				sortedMedian[i] = median[i];
//			}
//
//			for (int i = 49; i > 0; --i) {
//				for (int j = 0; j < i; ++j) {
//					if (sortedMedian[j] > sortedMedian[j + 1])
//					{
//						double dTemp = sortedMedian[j];
//						sortedMedian[j] = sortedMedian[j + 1];
//						sortedMedian[j + 1] = dTemp;
//					}
//				}
//			}
//			int dMedian;
//			dMedian = sortedMedian[48 / 2];
//			medianOutput[rows][cols] = dMedian;
//		}
//	}
//
//	int k = 0;
//
//	for (int i = 0; i < 480; ++i)
//	{
//		for (int j = 0; j < 640; ++j)
//		{
//			imageWriter->r[k] = medianOutput[i][j];
//			imageWriter->g[k] = medianOutput[i][j];
//			imageWriter->b[k] = medianOutput[i][j];
//			++k;
//		}
//	}
//	imageWriter->write("medianOutput.ppm");
//}


int calculateThreshold(uint8_t(&pArray)[480][640], int x, int y, int traverseAmountX, int traverseAmountY)
{
	// Set up variables for local and target thresholds
	int localThreshold = 0;

	// Calculate the local threshold in subgrid area specified by function parameters
	// Works by adding up value at each element in a subgrid provided by top left position and a maximum value to check across X and Y
	for (unsigned int row = x; row < x + traverseAmountX; ++row)
	{
		for (unsigned int column = y; column < y + traverseAmountY; ++column)
		{
			localThreshold += pArray[row][column];
		}
	}

	return localThreshold;
}


void drawBox(uint8_t(&pArray)[480][640], int x, int &y, int width, int height)
{
	// Draw a box using coordinates provided for top left corner and a width and height

	for (unsigned int row = x; row < x + height; row++)
	{
		for (unsigned int column = y; column < y + width; column++)
		{
			if (row == x || row == x + (height - 1))
			{
				pArray[row][column] = 50;
			}

			if (column == y || column == y + (width - 1))
			{
				pArray[row][column] = 50;
			}
		}
	}
}


void calculateSubGrid(uint8_t(&pArray)[480][640], int x, int &y, uint8_t(&pROIGrid)[480][640], int traverseAmountX, int traverseAmountY)
{
	// Calculate the threshold of a subgrid area and determine if it is a region of interest or not
	// If it is a region of interest then place a box on the area 

	// Set up variables for local and target thresholds
	int localThreshold = 0;
	const int targetThreshold = 700;
	
	// Get the threshold for this area
	localThreshold = calculateThreshold(pArray, x, y, traverseAmountX, traverseAmountY);

	int nextLocalThreshold = 0;
	bool endOfRow = false;
	int newY = y + traverseAmountY;

	// If the threshold of a subgrid is higher than the target, then we have found a region of interest
	if (localThreshold > targetThreshold)
	{
		// Create a box to be placed here, using the dimensions of the subgrid
		box* testBox = new box;
		testBox->x = x;
		testBox->y = y;
		testBox->w = 10;
		testBox->h = 10;

		// While we are not at the end of a horizontal row
		while (endOfRow == false)
		{
			// Check we aren't at the end of the image
			if (newY < 640)
			{
				// Calculate the threshold in the next subgrid to the right
				nextLocalThreshold = calculateThreshold(pArray, x, newY, traverseAmountX, traverseAmountY);

				// If the next threshold to the right is less than the target then we are past the edge of the cell
				if ((nextLocalThreshold < targetThreshold))
				{
					endOfRow = true;
				}
				else
				{
					// If we are still in the cell, move the column coordinate to check 10 to the next to check the next subgrid
					y += 10;
					newY += traverseAmountY;
					// Widen the width of the box by 10 to accomodate the next subgrid that is part of the region of interest
					int test = testBox->w;
					testBox->w = test += 10;
				}
			}
			else endOfRow = true;
		}
		// Push the newly created onto the vector
		boxVector.push_back(testBox);
	}
}


void drawFinalBoxes(uint8_t(&pArray)[480][640])
{
	// Place the box vector data onto the ROI grid so they can be drawn onto the original image data

	// If there are any boxes with width of 10 then it is noise that surpassed the threshold check, do not draw this box
	for (unsigned int index = 0; index < boxVector.size(); ++index)
	{
		if (boxVector[index]->w > 10)
		{
			drawBox(pArray, boxVector[index]->x, boxVector[index]->y, boxVector[index]->w, boxVector[index]->h);
		}
	}

	// Find where there are any double black lines in the array right next to each others column (i.e. black line touching another on the next row down)
	// Remove the double lines (removes lines inside of the mapped region of interest)
	for (unsigned int i = 0; i < 480; ++i)
	{
		for (unsigned int j = 0; j < 640; ++j)
		{
			if ((pArray[i + 9][j+1] == 50) && (pArray[i + 10][j] == 50))
			{
				pArray[i + 9][j-1] = 255;
				pArray[i + 10][j] = 255;
			}
		}
	}

	// For all lines part of regions of interest, change the value at those positions to 255 (will be used for drawing red onto original image data)
	for (unsigned int i = 0; i < 480; ++i)
	{
		for (unsigned int j = 0; j < 640; ++j)
		{
			if (pArray[i][j] == 50)
			{
				dataGrid[i][j] = 255;
			}
		}
	}

	// Write the red region of interest lines onto the original image data
	writeTGAFinal("final.tga", dataGrid);
}


void drawROIBoxes(uint8_t(&pArray)[480][640])
{
	// Create array that will hold colour value for final image
	uint_fast8_t ROIGrid[480][640];

	// Init every position to hold white/255
	for (unsigned int i = 0; i < 480; ++i)
	{
		for (unsigned int j = 0; j < 640; ++j)
		{
			ROIGrid[i][j] = 255;
		}
	}
	
	// Set traversal variables
	int currentRow = 0;
	int currentColumn = 0;
	int traverseAmountX = 10;
	int traverseAmountY = 10;

	// Loop until the last row is reached
	while (currentRow < 480)
	{
		// Loop until the last column of each row is reached
		while (currentColumn < 640)
		{
			// Calculate threshold in a sub-grid area and highlight regions of interest
			calculateSubGrid(pArray, currentRow, currentColumn, ROIGrid, traverseAmountX, traverseAmountY);

			// Increment traversal column
			currentColumn += traverseAmountY;
		}
		// Reset traveral column to start of row
		currentColumn = 0;

		// Increment traversal row
		currentRow += traverseAmountY;

		// If we are at row 470, as the array starts at 0 the final row is 479, so only increment by 9 instead of 10
		if (currentRow == 470)
		{
			traverseAmountX = 9;
		}

		// If we are at the end of a row, as the array starts at 0 the final column is 639, so only increment by 9 instead of 10
		if (currentColumn == 630)
		{
			traverseAmountY = 9;
		}
		else
		{
			// Set column traversal amount back to 10 when we complete a row
			traverseAmountY = 10;
		}
	}

	drawFinalBoxes(ROIGrid);

	// Write data to file
	writeTGA("ROIBoxes.tga", ROIGrid);
 }


void loadArray(const char* filePath, uint8_t (&pArray)[480][640])
{
	// Open file via MemoryMapping
	MemoryMapped file(filePath, MemoryMapped::WholeFile, MemoryMapped::SequentialScan);

	if (file.isValid() == false)
	{
		std::cout << "Invalid file path." << std::endl;
	}

	// Create a pointer to the data
	const unsigned char* pointer = file.getData();

	// Set up variables to traverse and read data
	std::string current;
	int row = 0;
	int column = 0;

	// Loop until end of file
	while (*pointer != '\0')
	{
		// If we are at a , then clear the string
		if (*pointer == ',')
		{
			current = "";
		}
		else
		{
			// Add current position to string
			current += *pointer;

			// Create new pointer from current pointer and increment it
			const unsigned char* pointer2 = pointer;
			++pointer2;

			// If second pointer is at a comma, or return character...
			if (*pointer2 == ',' || *pointer2 == 'r')
			{
				// Set the current position in the array to be the contents of the string
				pArray[row][column] = atoi(current.c_str());

				// Move to next column
				column++;
			}
		}

		// If current position is a return character we are at end of line (row)
		if (*pointer == '\r')
		{
			// Increment the row
			row++;
			// Set column counter back to 0
			column = 0;
			// Increment the current position pointer before we get to the start of the next line
			if (*pointer == '\n')
			{
				++pointer;
			}
		}
		// Increment current position pointer
		pointer++;
	}

	// Close file once all data has been loaded into array
	file.close();
}


void writeTGA(const char* fileName, uint8_t(&pArray)[480][640])
{
	// Colour variable goes into each pixel of the image
	Colour colour;

	// Loop for size of image
	for (int x = 0; x < 480; x++)
	{
		for (int y = 0; y < 640; y++)
		{
			// Set red, green and blue value of colour at current position to value held at that position in the array
			// array x position is [480 - x] so image is not upside down (this is to do with the way pixels are stored in TGA format, where they start at the bottom rather than top of the image)
			colour.r = pArray[480 - x][y];
			colour.g = pArray[480 - x][y];
			colour.b = pArray[480 - x][y];
			colour.a = 255;
			// Set the colour at the current position
			img->setPixel(colour, x, y);
		}
	}

	// Write the data to file
	img->WriteImage(fileName);
}


void writeTGAFinal(const char* fileName, uint8_t(&pArray)[480][640])
{
	// Colour variable goes into each pixel of the image
	Colour colour;

	// Loop for size of image
	for (int x = 0; x < 480; x++)
	{
		for (int y = 0; y < 640; y++)
		{
			// Set red, green and blue value of colour at current position to value held at that position in the array
			// array x position is [480 - x] so image is not upside down (this is to do with the way pixels are stored in TGA format, where they start at the bottom rather than top of the image)

			if (pArray[480 - x][y] == 255)
			{
				colour.r = pArray[480 - x][y];
				colour.g = 0;
				colour.b = 0;
			}
			else
			{
				colour.r = pArray[480 - x][y];
				colour.g = pArray[480 - x][y];
				colour.b = pArray[480 - x][y];
			}
			colour.a = 255;
			// Set the colour at the current position
			img->setPixel(colour, x, y);
		}
	}

	// Write the data to file
	img->WriteImage(fileName);
}


int main(int argc, char* argv[])
{
	// Load the data from specified file into an array
	loadArray("data.csv", dataGrid);

	// Perform Sobel operations on the specified array
	sobel(dataGrid);

	drawROIBoxes(output);
	

	//median();
	// Used for high resolution timing
	/*auto begin = std::chrono::high_resolution_clock::now();
	uint32_t iterations = 100;
	for (uint32_t i = 0; i < iterations; ++i)
	{
	auto beginIteration = std::chrono::high_resolution_clock::now();

	CODE TO TEST GOES HERE

	auto endIteration = std::chrono::high_resolution_clock::now();

	std::cout << std::chrono::duration_cast<std::chrono::milliseconds>(endIteration - beginIteration).count() << "ms for iteration: " << i << std::endl;
	}
	auto end = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
	std::cout << duration << "ms total for: " << iterations << " iterations, average: " << duration / iterations << " ms" << std::endl;*/

	return 0;
}
