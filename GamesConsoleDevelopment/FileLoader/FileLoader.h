#pragma once

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>

class FileLoader
{
public:
	FileLoader();
	~FileLoader();

	void LoadFile(const char* file, std::vector<std::vector<std::string>>&);

private:
	void OpenFile(const char* file);
	void CloseFile();
	const void HasOpenedSuccesfully();

private:
	std::fstream currentFile;
	char i;
	char j;
};