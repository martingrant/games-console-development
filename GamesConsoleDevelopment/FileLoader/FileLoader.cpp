#include "FileLoader.h"


FileLoader::FileLoader()
{
	i = 0;
	j = 0;
}


FileLoader::~FileLoader()
{
}

#pragma region Functions For Open & Closing a File
/*
* Used to check if the file has opened succesfully.
* If it has an appropriate message is displayed.
* If it cannot be opened then, again, an appropriate error message is displayed
*/
const void FileLoader::HasOpenedSuccesfully()
{
	if (currentFile.is_open())
	{
		std::cout << "File opened Succesfully" << std::endl;
	}
	else{ std::cout << "File Could Not Be Opened" << std::endl; }
}

/*
* Opens a file giving write and read access.
* After calling the function to open the file it checks if the file
* has been opened succesfully.
*
* @param const char* file - This is the file that is to be opened.
*/
void FileLoader::OpenFile(const char* file)
{
	currentFile.open(file, std::ios::in | std::ios::out);
	HasOpenedSuccesfully();
}

void FileLoader::LoadFile(const char* file, std::vector<std::vector<std::string>>& dataGrid)
{
	if (currentFile.is_open() == false){ OpenFile(file); }
	std::string rowString;
	std::string columnString;
	std::stringstream stringstream(rowString);
	std::vector<std::string> row;
	while (currentFile)
	{
		if (!getline(currentFile, rowString)) break;

		while (stringstream)
		{
			row.clear();

			if (!getline(stringstream, columnString, ',')) break;

			row.push_back(columnString);
		}

		dataGrid.push_back(row);
	}
	if (currentFile.eof())
	{
		std::cout << "File loaded." << std::endl;
		CloseFile();
	}
}
/*
* Closes a file.
*/
void FileLoader::CloseFile()
{
	currentFile.close();
}
#pragma endregion