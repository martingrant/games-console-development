######################

ImageViewer.exe submitted by B00221736 and Bxxxxxx
for Games Console Development (COMP10034) at
University of the West of Scotland.


#####################
Instructions:

1)	Please ensure there is a folder named "Images" in the same directory as ImageViewer.exe and it's accompanying program files. If there is not you will have to create one.

2)	Please ensure you move the images you wish to view into the Images folder. This is where the program will look for images to load. There should already be an Images folder with a test image to test.

3)	Open ImageViewer.exe to run the program.

4)	Use Left and Right arrow keys to select different images to view.

5) 	The file name of the current image will be shown at the top of the screen.

6)	Escape key or closing the window will close the program.


###################################

Credits:

MavenPro-Regular.ttf font provided by http://www.google.com/fonts/specimen/Maven+Pro
Licensed under: http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL

Program written in C++ using SDL. Licensing can be found at: https://www.libsdl.org/license.php

Code to search a directory for files provided by user "dehdar" from cplusplus.com forum: http://www.cplusplus.com/forum/general/3221/#msg13384