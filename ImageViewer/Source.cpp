#include <iostream>
#include <string>
#include <Windows.h>
#include <vector>

#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>

SDL_Window* window;
SDL_Renderer* renderer;
SDL_Event event;
std::vector<SDL_Texture*> textureVector;
std::vector<std::string> textureNameVector;
int currentImage = 0;
bool running = true;
TTF_Font *font;

void renderText(TTF_Font* font, const char* text, int posX, int posY)
{
	// Red colour for text
	SDL_Colour test = { 255, 0, 0 };
	// Create surface for text
	SDL_Surface *surf = TTF_RenderText_Blended(font, text, test);

	// Set a rect to position the text on screen
	SDL_Rect rect;
	rect.x = posX;
	rect.y = posY;
	rect.w = surf->w;
	rect.h = surf->h;

	// Create texture and add the surface data to it
	SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surf);

	// Delete the surface
	SDL_FreeSurface(surf);

	// Render the text texture
	SDL_RenderCopy(renderer, texture, NULL, &rect);

	// Destroy the texture
	SDL_DestroyTexture(texture);
}


void loadImage(unsigned int imageID, const char* filePath)
{
	// Load texture into vector
	textureVector.push_back(IMG_LoadTexture(renderer, filePath));
}


void update()
{
	// Check for events
	while (SDL_PollEvent(&event) != 0)
	{
		if (event.type == SDL_WINDOWEVENT)
		{
			// If window is closed, shut down program
			if (event.window.event == SDL_WINDOWEVENT_CLOSE)
			{
				running = false;
				break;
			}
		}
		else if (event.type == SDL_KEYDOWN)
		{
			// Decrement image view counter when left arrow is pressed, cannot be less than 0
			if (event.key.keysym.scancode == SDL_SCANCODE_LEFT)
			{
				if (currentImage > 0)
				{
					currentImage--;
				}
			}
			// Increment image view counter when right arrow is pressed, cannot be greater than size of texture vector - 1
			else if (event.key.keysym.scancode == SDL_SCANCODE_RIGHT)
			{
				if (currentImage < textureVector.size() - 1)
				{
					currentImage++;
				}
			}
			// Close program if escape key is pressed
			else if (event.key.keysym.scancode == SDL_SCANCODE_ESCAPE)
			{
				running = false;
				break;
			}
		}
	}
}


void render()
{
	// Clear screen
	SDL_RenderClear(renderer);

	// Only render a texture if any has been loaded (so if there is something in the texture vector)
	if (textureVector.size() > 0)
	{
		SDL_RenderCopy(renderer, textureVector[currentImage], NULL, NULL);
		renderText(font, textureNameVector[currentImage].c_str(), 10, 10);
		renderText(font, "Use Left and Right Arrow keys to switch to different images.", 10, 450);
	}
	// Else tell user images folder can't be found
	else
	{
		renderText(font, "Images folder not found. OR no files in Images folder.", 10, 10);
		renderText(font, "Please close this program then create a folder named 'Images',", 10, 30);
		renderText(font, "in the same folder as this program.", 10, 50);
		renderText(font, "Place your image files in the 'Images' folder. Then try to run the program.", 10, 70);
	}
	
	// Update screen
	SDL_RenderPresent(renderer);
}


void loadImages()
{
	// The code directly below is taken from http://www.cplusplus.com/forum/general/3221/#msg13384.
	// Code is provided by user "dehdar" on cplusplus.com, it has not been written by the submitter of this project.
	// This is used to search for files in a given folder.

	// It will check to see if there is an Images folder in the same directory as the program exe
	// It will then load all file names into a vector

	WIN32_FIND_DATA data;
	HANDLE h = FindFirstFile(L"Images\\*.*", &data);
	std::vector<const char*> vector;
	if (h != INVALID_HANDLE_VALUE)
	{
		do
		{
			char*   nPtr = new char[lstrlen(data.cFileName) + 1];
			for (int i = 0; i < lstrlen(data.cFileName); i++)
				nPtr[i] = char(data.cFileName[i]);

			nPtr[lstrlen(data.cFileName)] = '\0';

			vector.push_back(nPtr);

		} while (FindNextFile(h, &data));
	}

	FindClose(h);

	// Start from second index and add file name onto "Images/" string
	// Send the file path to the loadImage function to load it into a texture
	// Add the name into a vector to be displayed on screen later

	// Start from second index, links to parent folder is added to the vector, need to ignore this
	for (int i = 2; i < vector.size(); ++i)
	{
		std::string test = "Images/";
		test += vector[i];
		loadImage(i, test.c_str());
		textureNameVector.push_back(test);
	}
}



int main(int argc, char *args[])
{
	// Init SDL
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		std::cout << "SDL_Init: " << SDL_GetError() << std::endl;
	}

	// Init SDL_TTF
	if (TTF_Init() != 0)
	{
		std::cout << "TTF_Init: " << SDL_GetError() << std::endl;
	}

	// Create Window
	window = SDL_CreateWindow("TGA Viewer", SDL_WINDOWPOS_CENTERED_DISPLAY(0), SDL_WINDOWPOS_CENTERED_DISPLAY(0), 640, 480, SDL_WINDOW_SHOWN);

	// Create Renderer
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

	// Set window clear colour
	SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

	// Open font file
	font = TTF_OpenFont("MavenPro-Regular.ttf", 16);

	// Load the images found in the /Images folder
	loadImages();

	// Application loop
	// Running is flipped to false when escape key is pressed or window is closed
	while (running == true)
	{
		update();
		render();
	}

	// Destroy textures inside of texture vector
	for (unsigned int index = 0; index < textureVector.size(); ++index)
	{
		SDL_DestroyTexture(textureVector[index]);
	}

	// Clear texture vectors
	textureNameVector.clear();
	textureVector.clear();

	// Shut down SDL.
	TTF_CloseFont(font);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();

	return 0;
}